PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/system/etc/permissions/privapp-permissions-oem.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-oem/xml \
    $(LOCAL_PATH)/system/etc/sysconfig/hiddenapi-package-whitelist-oem.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist-oem.xml

PRODUCT_PACKAGES += \
    OnePlusCamera_sdm845 \
    OnePlusCameraService_sdm845 \
    OnePlusGallery_sdm845
